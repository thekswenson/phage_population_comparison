# Reproducing the results from "On the comparison of bacteriophage populations"
(*These instructions assume a system running Ubuntu 20.04*)

## Installation

These are steps to download the code and install the necessary dependencies.

1. Install dependencies using apt:
    `sudo apt install git python3-networkx snakemake python3-biopython python3-scipy`
2. Clone the [repository](https://bitbucket.org/thekswenson/phagerecombination):
    `git clone --depth 1 https://bitbucket.org/thekswenson/phagerecombination.git`

## Reproducing results

There is a Snakemake file in the `data/` directory that will reproduce all of the results from this paper. To run it:

1. Move to the `data/` directory with `cd phagerecombination/data`. 
2. Run snakemake with `snakemake --configfile snakeconfig/default.yaml` (use `-j n` with snakemake to use parallelism, *n* being the number of processors -- *there is a significant amount of parallelism in the program*).

The output is found in the directory `data/lacto_output/`. Details of the recombination scenarios are found in files of the form `data/lacto_output/*output.txt`, while scenario summaries are found in files of the form `data/lacto_output/summary.txt`.


## Recreating the factory input modules and variants

*WARNING*: this step takes roughly 20Gb of RAM.

The data in the directory [`phagerecombination/data/lacto/`](https://bitbucket.org/thekswenson/phagerecombination/src/master/data/lacto/) was created using the [Alpha](https://bitbucket.org/thekswenson/alpha) bacteriophage aligner. To recreate the modules and variants for this dataset, follow these steps:

  1. Install the Alpha aligner ([https://bitbucket.org/thekswenson/alpha](https://bitbucket.org/thekswenson/alpha))
  2. Download the [fasta file](https://bitbucket.org/thekswenson/phagerecombination/src/master/data/lacto/CheeseFactoriesDataSet.fasta)  to the alpha installation directory.
  3. Run Alpha on the fasta file `alpha --modules -f CheeseFactoriesDataSet.fasta > modules.txt`.

The result is a file with all of the modules and variants.  The file must then manually be split into the appropriate factory files.